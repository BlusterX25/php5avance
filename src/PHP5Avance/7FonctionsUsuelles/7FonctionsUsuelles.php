<?php    
    echo "Chapitre 7 : Fonctions usuelles";
    echo "<br />----------------------------------------------------<br /><br />";
    
    // Fonctions mathématiques
    echo "<br />* max : " . max(1,2,3,4,5,6,7);
    echo "<br />* max : " . max(array(1,2,3,4,5,6));
    echo "<br />* Arrondi round(3.6,0) : " . round(3.6,0);
    echo "<br />* Arrondi round(1.95583,2) : " . round(1.95583, 2); 
    echo "<br />* Arrondi round(1241757,-3) : " . round(1241757, -3);
    
    echo "<br />* ceil(7.2) : " . ceil(7.2); // Arrondi supérieur
    echo "<br />* floor(7.2) : " . floor(7.2); // Arrondi inférieur
    echo "<br />* rand(1,5) : " . rand(1, 5);
    echo "<br />* mt_rand(1,5) : " . mt_rand(1,5); // A préconiser par rapport à rand()
    
    // Travailler sur différentes bases
    // base_convert(nombre, frombase, tobase)
    // bindec() : convertit de binaire en décimal
    // decbin() : convertit de decimal en binaire
    // dechex() : convertit de décimal en héxadécimal
    // decoct() : convertit de décimal en octal
    // bin2hex() : convertit de binaire en héxadécimal
    
    // Fonctions de date
    $format = 'd-m-y, H:i:s';
    $date   = date($format);
    echo "<br />* date : " . $date;
    $timestamp = strtotime('1976-11-16');
    echo "<br />* timestamp : " . $timestamp;
    echo "<br />* +2jours : " . strtotime('+2 days', mktime(0,0,0,10,6,1976)); // Ajoute deux jour au 6/10/1976
    echo "<br />* next month : " . strtotime('next month');
    echo "<br />* date('d/M/Y', timestamp) : " . date('d/m/Y', mktime(0,0,0,11,12,2013));
    // strftime() permet également de transformer timestamp en date
    setlocale(LC_TIME, 'fr_FR');
    echo "<br />* strftime() : " . strftime('%A %d %B %Y', strtotime(('now')));
    
    // checkdate(mois, jour, année) : permet de vérifier la validité d'une date
    // time() : renvoie le timestamp actuel
    // microtime() : renvoie le timestamp actuel mais va jusqu'au millionnième de seconde
    $tps1 = microtime(true);
    sleep(1);
    $tps2 = microtime(true);
    $tps  = $tps2 - $tps1;
    echo "<br />* [tps 1 -> tps 2] = [" . $tps1 . " -> " . $tps2 . " ]";
    echo "<br />* Le temps necessaire a l'execution des traitements est $tps";
    
    // Fonctions réseau
    if (checkdnsrr('anaska.com')) {
        echo "<br />* checkdnsrr('anaska.com') =>Le DNS existe ";
    } else {
        echo "<br />* checkdnsrr('anaska.com') => Le dns n'existe pas";
    }

    $result = dns_get_record('google.fr');
    echo "<br />* dns_get_record('google.fr')";
    echo "<pre>";
    print_r($result);
    echo "</pre>";
    $hote = gethostbyaddr('173.194.45.63');
    echo "<br />* gethostbyaddr('173.194.45.63') => hote de IP google.fr : " . $hote;
    
    // Fonctions de chiffrement
    // Méthode de hachage crc32()
    // Méthode de hachage md5() : plus complexe que CRC32
    // md5_file() calcule le md5 d'un fichier
    // sha1() et sha1_file() similaire à md5() et md5_file() mais renvoie une chaine légèrement plus longue
    // crypt() permet d'authentifier quelqu'un avec un hachage dérivé du codage DES
    
    // Exécution de code
    // register_shutdown_function($function) : PHP l'enregistre et l'éxécute juste avant de terminer le script
    // eval() permet d'éxécuter une chaine de code
    
