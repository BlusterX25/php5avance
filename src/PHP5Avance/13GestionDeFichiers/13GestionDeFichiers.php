<?php
    echo "<br />Chapitre 13 : Gestion de fichiers";
    echo "<br />--------------------------------------------------------";
    
    $pathFichier    = '../../../app/Resources/files/monfichier.txt';
    
    $contenuFichier = file_get_contents($pathFichier);
    echo "<br />* Contenu fichier file_get_contents() : <br />" . $contenuFichier;
    
    echo "<br />* Contenu fichier readfile() : <br />";
    if (!$read = readfile($pathFichier)) {
        echo "<br />ERREUR readfile a la ligne " . __LINE__;
        exit(-1);
    }
    
    echo "<br />* Contenu fichier fpassthru() : <br />";
    $fp = fopen($pathFichier, 'r');
    if (!$fp = fopen($pathFichier, 'r')) {
        echo "<br />ERREUR lors de l'ouverture du fichier a la ligne " . __LINE__;
        exit(-1);
    }
    if (!$fpass = fpassthru($fp)) {
        echo "<br />ERREUR fpassthru a la ligne " . __LINE__;
        exit(-1);
    }
    fclose($fp);
    
    echo "<br />* Contenu fichier avec file() : <br />";
    $tab = file($pathFichier);
    print_r("<pre>");print_r($tab);print_r("</pre>");
    
    echo "<br />* Contenu fichier de configuration parse_ini_file() : <br />";
    $pathIni    = '../../../app/Resources/files/monfichier.ini';
    if (!$tabIni = parse_ini_file($pathIni)) {
        echo "<br />ERREUR dans parse_ini_file a la ligne " . __LINE__;
        exit(-1);
    }
    print_r("<pre>");print_r($tabIni);print_r("</pre>");
    
    
    echo "<br />* PHP_OS : " . PHP_OS;
    
    echo "<br />* Exploiter fichier CSV : <br />";
    $pathCSV    = '../../../app/Resources/files/monfichier.csv';
    if (!$fpCSV = fopen($pathCSV, 'r')) {
        echo "<br />EREUR lors de l'ouverture de fichier a la ligne " . __LINE__;
        exit(-1);
    }
    while ($tabCSV = fgetcsv($fpCSV, 1000, ',')) {
        print_r("<pre>");print_r($tabCSV);print_r("</pre>");
    }
    fclose($fpCSV);
    
    // Ecriture rapide
    $pathEcriture = '../../../app/Resources/files/monfichierEcriture.txt';
    $contenu      = "Contenu dans le fichier";
    if (!file_put_contents($pathEcriture, $contenu, FILE_APPEND | LOCK_EX)) {
        echo "<br />ERREUR dans file_put_contents() a la ligne " . __LINE__;
        exit(-1);
    }
    if (!$resultPu = file_get_contents($pathEcriture)) {
        echo "<br />ERREUR dans file_get_contents() a la ligne " . __LINE__;
        exit(-1);
    }
    
    echo "<br />* Ce que l'on vient d'&eacute;crire avec file_put_contents() : <br />";
    echo $resultPu;
    
    // Lecture d'un fichier
    echo "<br />* Lire caractère par caractère";
    if (!$fp = fopen($pathEcriture, 'rb')) {// rb pour read et binaire pr les fichiers bianire qd on est sur Windows
        echo "<br />ERREUR fopen a la ligne " . __LINE__;
        exit(-1);
    }
    $caractere = fgetc($fp);
    echo "<br />Caractère lu : " . $caractere;
    $ligne = fgets($fp,1024);
    echo "<br />Ligne lu : " . $ligne;
    
    // rtrim($chaine, "\r\n") pour enlever le caractère de fin de ligne
    // On peut écrire aussi avec fputs($fp, $texte);
    
    echo "<br />* Lire un fichier entier : <br />";
    if (!$fp = fopen($pathFichier, 'rb')) {// rb pour read et binaire pr les fichiers bianire qd on est sur Windows
        echo "<br />ERREUR fopen a la ligne " . __LINE__;
        exit(-1);
    }
    $s = fread($fp, 42);
    echo "<br />&nbsp;&nbsp;&nbsp;Les 42 premiers caracteres sont : " . $s;
    $s = fread($fp, filesize($pathFichier));
    echo "<br />&nbsp;&nbsp;&nbsp;Lecture du fichier en entier : " . $s;
    
    echo "<br />/* Ecriture dans un fichier";
    echo "<br />* Lire caractère par caractère";
    if (!$fp = fopen($pathEcriture, 'a+')) {
        echo "<br />ERREUR fopen a la ligne " . __LINE__;
        exit(-1);
    }
    fwrite($fp, "PHP 5 avancé"); // La tailler en octet peut être fourni en 3ème argument
    
    // $rewind($fp) => Place le curseur au début du fichier
    // fseek($fp, 42) => positionne le curseur au 42 ème octet du fichier
    // feof($fp) => Détecte la fin du fichier
    
    // fflush($fp) => Demande à PHP de forcer l'écriture de ce qu'il y a dans le tampon
    
    // flock($fp, LOCK_SH) => Verrou partagé sur le fichier (bon pour la lecture)
    // flock($fp, LOCK_EX) => Verrou exclusif sur le fichier (non pour l'écriture)
    // flock($fp, LOCK_UN) => Libération du verrour 
    
    // Quand on ouvre un fichier avec option w, le fichier est tronqué. Pour éviter la troncature,
    // on peut l'ouvrir avec r+ et faire ftruncate($fp) après le flock()
    
    if (!$fichierTmp = tempnam('/tmp','')) {
        echo "<br />Erreur sur tempnam() a la ligne " . __LINE__;
        exit(-1);
    }
    echo "<br />* Nom fichier temporaire : " . $fichierTmp;
    //rename($fichierTmp, '../../../app/Resources/files/'); => déplacement de fichier
    //copy("fichierSource","fichierDestination"); => copie de fichier
    
    //file_exists($fichier) => tester l'existence d'un fichier
    
    $time    = mktime(0,0,0,10,10,2003);
    $fichier = '../../../app/Resources/files/monfichierTouch.txt';
    //touch($fichier, $time); // Modifie la date de modification du fichier et le créé s'il n'existe pas
    
    
    // Effacement de fichier avec unlink($fichier)
    
    // Parcourir un répertoire
    echo "<br />* Parcourir un repertoire (méthode objet) :";
    $dir = dir('../../../app/Resources/files/');
    $fichiers = array();
    while ($nom = $dir->read()) {
        $fichiers[] = $nom;
    }
    echo "<br />Tous les fichiers du répertoire : ";
    print_r("<pre>");print_r($fichiers);print_r("</pre>");
    $dir->close();
    
    echo "<br />* Parcourir un repertoire (méthode procédurale) : ";
    $dirProc = opendir('../../../app/Resources/files/');
    $fichiersProc = array();
    while ($nomProc = readdir($dirProc)) {
        $fichiersProc[] = $nomProc;
    }
    print_r("<pre>");print_r($fichiersProc);print_r("</pre>");
    
    echo "<br />* Parourir un répertoire (méthode rapide avec masque => glob())";
    $fichiersGlob = glob('../../../app/Resources/files/*');
    print_r("<pre>");print_r($fichiersGlob);print_r("</pre>");
    // Contrairement aux deux autres méthodes glob() ne retourne pas les fichiers cachés notament "." et ".."
    
    echo "<br />* Repertoire courant : " . getcwd();
    echo "<br />* On monte d'un niveau ." . chdir('..');
    echo "<br />* Repertoire courant : " . getcwd();
    
    // mkdir() pour créer un repertoire avec paramètre optionnel 0777 pr les droits
    // rmdir() pour changer de repertoire
    
    // Informations sur les fichiers
    
    // clearstatcach() => Vide le cache de statistique de PHP
    // $timeC = filectime($pathFichier);
    // echo "<br />* Date de création du fichier : " . $timeC;
    
    //$timeM = filemtime($pathEcriture);
    //echo "<br />* Date de modification du fichier : " . $timeM;
    
    /*$stat = stat($pathEcriture);
    echo "<br />Stat du fichier : ";
    print_r("<pre>");print_r($stat);print_r("</pre>");*/
    // fstat() => Comme stat() mais prend en paramètre un descripteur de fichier
    
    // disk_free_space($repertoire) => espace disponible
    
    $cheminReel = realpath($pathFichier);
    echo "<br />* Chemin réel : ";
    print_r($cheminReel);
    
    // is_dir(), is_file(), is_link()
    // filetype() => renvoie le type du fichier (fifo, char, dir, block, link, file, unknown)
    // readlink() => lire lien symbolique
    // linkinfo() => test si la cible est un lien symbolique et renvoie null sinon
    // lstat() => statistiques d'un lien symbolique
    
    