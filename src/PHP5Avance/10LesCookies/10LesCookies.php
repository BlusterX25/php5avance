<?php
/**
 * La durée de vie d'un cookie se limite à une session du navigateur quand il n'y a pas de paramétre de durée de vie
 */
    echo "Chapitre 10 : Les cookies";
    echo "<br />---------------------------";
    
    
    if (filter_has_var(INPUT_COOKIE, 'langage')) {
        $langage = filter_input(INPUT_COOKIE, 'langage');
        echo "<br /><br />Avec filter : Le cookie a ete initialise : " . $langage;
    } else {
        echo "<br /><br />Avec filter : Le cookie n'a pas ete cree";
        setcookie('langage', 'PHP version 5 dans filter');
    }
    
    if (isset($_COOKIE['langage'])) {
        echo "<br /><br />Avec COOKIE : Le cookie a ete initialise : " . $_COOKIE['langage'];
    } else {
        echo "<br /><br />Avec COOKIE : Le coojie n'a pas ete envoye";
        setcookie('langage', 'PHP version 5 dans COOKIE');
    }
    
    // Si on veut mettre un tableau ou un type complexe dans un cookie il faut utiliser la méthode serialize() sur l'objet en question
    // Si on veut recupérer le type complexe dans un cookie on utilise la méthode unserialize()
    //setcookie($nom, $valeurCookie, $dureeDeVieCookie, $cheminAccesPourLequelCookieAutorise, $nomDeDomainePourLequelLeCookieEstRenvoye, $bool)
    // Le paramètre $bool ci-dessus permet de d'éviter de diffuser un cookie avec contenu sensible sur une connexion non sécurisée
    
    // Pour détruire une cookie on fait un unset($_COOKIE['valeur'])
    
?>

