<?php
	echo "Chapitre 3 : Les structures de base<br />";
	echo "------------------------------------------------------<br />";
	# Un autre commentaire sur une ligne
	
	// Enchainement des instructions
	$a = 5;$b = 3;
	$c =
		"PHP5"; echo "$c<br />";
	
	// Portée des variables 
	echo "<br /><br />Port&eacute;e des variables";
	function gargarise ($var1, $var2) {
		global $titre, $auteur;
		echo $var1, $GLOBALS['titre'], $var2, $GLOBALS['auteur'];
		echo "<br />" . $var1, $titre, $var2, $auteur;
	}
	$titre  = 'EL DESDICHADO';
	$auteur = 'Gerard de Nerval';
	gargarise('Mon po&egrave;me pr&eacute;f&eacute;r&eacute; est ', '<br />Son auteur est ');
	
	// Test d'existence
	echo "<br /><br />Test d'existence";
	$i          = 'test';
	$chaineVide = "";
	$espace     = " "; 
	echo "<br />isset(i) : " . isset($i);
	echo "<br />isset(j) : " . isset($j);
	echo "<br />isset()  : " . isset($chaineVide);
	echo "<br />empty()  : " . empty($chaineVide);
	echo "<br />isset(espace) : " . isset($espace);
	echo "<br />empty(espace) : " . empty($espace);
    unset($i);
    echo "<br />isset(i) apr&eacute;s unset : " . isset($i);
    
    // Variables dynamiques
    echo "<br /><br />Varibles dynamiques";
    $cd      = 15;
    $dvd     = 30;
    $produit = "dvd";
    echo "<br />Produit : " . $$produit;
    echo "<br />Produit : " . ${$produit};
    echo "<br />Produit : " . ${"dv" . "d"};
    
    // Constantes 
    echo "<br /><br />Constantes";
    define("CONST_NOM", "PHP Avanc&eacute;e");
    echo "<br />constane NOM : " . CONST_NOM;
    const CONST_PRENOM = "Asterios";
    echo "<br />constante pr&eacute;nom : " . CONST_PRENOM;

    // Types de données
    echo "<br /><br />Types de donn&eacute;es";
    echo "<br />gettype(chaine) : " . gettype("Chaine de caract&egrave;re");
    $var = 12;
    if (is_string($var)) {
        echo "<br />Variable de type string";
    } else {
        echo "<br />Autre type";
    }
    $bool = TRUE;
    echo "<br />TRUE : " . $bool;
    $bool = FALSE;
    echo "<br />FALSE : " . $bool;
    $nombre = 3.141559;
    echo "<br />PI : " . $nombre;
    $nombre = 5e7;
    echo "<br />5e7 : " . $nombre;
    
    // Interprétation des variables
    $objet            = new stdClass();
    $objet->propriete = "livre";
    $chaine           = "Son {$objet->propriete} a d&eacute;clench&eacute; la l&eacute;gende"; 
    echo "<br />" . $chaine;
    
    // Caractère d'échappement (ou protection)
    echo "<br />Le livre le \"premz\" a d&eacute;clench&eacute; la l&eacute;gende (\\)";
    
    // Syntaxe hérédoc
    $nom = "Mukulumpa";
    $texte = <<<syntaxeheredoc
    Un exemple de cha&icircne s'&eacute;talant
    sur plusieurs lignes avec la synstaxe heredoc<br />. Mon nom est 
    contenu dans la variable \$nom : "$nom".<br />
    Je peux ecrire des caracteres speciaux : 'A' majuscule : \x41
syntaxeheredoc;
    
    echo "<br />" . $texte;
    
    // Syntaxe nowdoc
    $texte = <<<syntaxenowdoc
    Un exemple de cha&icircne s'&eacute;talant
    sur plusieurs lignes avec la synstaxe nowdoc<br />. Mon nom est 
    contenu dans la variable \$nom : "$nom".<br />
    Je peux ecrire des caracteres speciaux : 'A' majuscule : \x41<br />
    Cependant la variable $nom et \x41 ne seront pas interpretes.
syntaxenowdoc;

    echo "<br /><br />" .$texte;

    // Transtypage
    echo "<br /><br /> Transtypage<br />";
    echo "3" + 1; // Affiche 4
    echo "<br />";
    echo 1 + "-1.3e3"; // Affiche -1299
    echo "<br />";
    echo 1 + "3 petits cochons"; // Affiche 4
    echo "<br />";
    echo 1 + "petits cochons"; // Affiche 1
    echo "<br />" . gettype("12");
    echo "<br />" . gettype(12);
    $chaine = "12";
    settype($chaine,'integer');
    echo "<br />" . gettype($chaine);
