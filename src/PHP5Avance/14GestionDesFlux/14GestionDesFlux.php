<?php
    echo "<br />Chapitre 14 : Gestion des flux";
    echo "<br />---------------------------------------------------";
    
    if (PHP_OS == 'Linux') {
        echo "<br /><br />* shell_exec('ls -la') : <br />";
        $rep = shell_exec('ls -la');
        print_r("<pre>");print_r($rep);print_r("</pre>");
    } else {
        echo "<br /><br />* shell_exec('dir') : <br />";
        $rep = shell_exec('dir');
        print_r("<pre>");print_r($rep);print_r("</pre>");
    }
    
    // exec('./programme', $result, $codeRetour) => execute directement la commande

    if (PHP_OS == 'Linux') {
        echo "<br /><br />* passthru('ls -la') : <br />";
        $rep = nl2br(passthru('ls -la'));
        print_r("<pre>");print_r($rep);print_r("</pre>");
    } else {
        echo "<br /><br />* passthru('dir') : <br />";
        $rep = passthru('dir');
        print_r("<pre>");print_r($rep);print_r("</pre>");
    }
    
    if (PHP_OS == 'Linux') {
        echo "<br /><br />* system('ls -la') : <br />";
        $rep = system('ls -la', $retour);
        print_r("<pre>");print_r($rep);print_r("</pre>");
    } else {
        echo "<br /><br />* system('dir') : <br />";
        $rep = system('dir');
        print_r("<pre>");print_r($rep);print_r("</pre>");
    }
    
    // Ouverture de flux
    $fp = popen('ipconfig /all', 'r');
    echo "<br />* popen('ipconfig /all') : <br />";
    print_r("<pre>");print_r(fread($fp, 5000));print_r("</pre>");
    
    // $proc= proc_open($commande, $flux, $descripteurs)
    // proc_get_status($proc) => Renvoie un tableau des infos
    // proc_close($proc)
    // escapeshellcmd() et escapeshellarg() pour protéger les caractères interprétables
    
    // Sockets 
    
    $timeout = 10;
    // Se connecte pour ouvrir une session telnet
    if (!$fp = fsockopen('192.168.2.6', 23, $erno, $ermsg, $timeout)) {
        echo "<br />ERREUR $erno : $ermsg";
        //exit(-1);
    }
        echo "<br />* fsockopen, fp : ";
    print_r("<pre>");print_r($fp);print_r("</pre>");
    
    
    // Se connecte au serveur Web de php.net
    $timeout = 10;
    echo "<br />* fsockopen, fp : ";
    $fp = fsockopen('www.php.net', 80, $erno, $ermsg, $timeout);
    stream_set_blocking($fp, TRUE); // Utilisation des socket en mode bloquant
    // Si la connexion est non bloquante il faudra faire refaire une demande de lecture plus tard
    
    // Si le serveur ne donne plus de nouvelles au bout de 10 secondes on considère que la connexion est perdu
    stream_set_timeout($fp,10);
    if (!$fp) {
        echo "<br />ERREUR $erno : $ermsg";
        exit(-1);
    }
    print_r("<pre>");print_r($fp);print_r("</pre>");
    
    // Lecture et Ecriture
    fputs($fp, "GET / HTTP/1.1\r\n");
    fputs($fp, "Host: www.php.net\r\n");
    fputs($fp, "Connection: close\r\n");
    fputs($fp, "\r\n");
    
    // Connaitre l'état de la socket, c'est l'équivalent de proc_get_status()
    $info = stream_get_meta_data($fp);
    echo "<br />* stream_get_meta_data() : ";
    print_r("<pre>");print_r($info);print_r("</pre>");
    
    
    while(!feof($fp)) {
        echo fgets($fp, 128);
    }
    fclose($fp);
    
    // Obtenir la liste des flux gérés
    $fp = stream_get_wrappers();
    echo "<br />* stream_get_wrappers() => Liste d'abstraction d'accès aux fichiers :";
    print_r("<pre>");print_r($fp);print_r("</pre>");
    
    // Liste des transports réseaux
    $fp = stream_get_transports();
    echo "<br />* stream_get_transports() => Liste des transports gérés :";
    print_r("<pre>");print_r($fp);print_r("</pre>");
    
    $fp =stream_socket_client('tcp://192.168.1.12:80');
    echo "<br />* stream_socket_client() : ";
    print_r("<pre>");print_r($fp);print_r("</pre>");
    
    // stream_copy_to_stream() permet de connecter deux flux, PHP lira le descripteur pr écrire dans le second
    
    // stream_get_line($fp, 1024, "\r\n") => Lire une une ligne de tailler 1024
    
    // stream_set_write_buffer() => Permet de définir la taille du tampon sur un flux
    
    // fflush() pour vider le tampon du flux
    
    /*
    $server =stream_socket_server('tcp://0.0.0.0:80');
    echo "<br />* stream_socket_server() : ";
    print_r("<pre>");print_r($server);print_r("</pre>");
    // Une fois le serveur créé on attend la connexion 10 secondes 
    // avec $fp = stream_socket_accept($server, 10);
    */
    
    // stream_filter_append() ou stream_filter_prepend() pr appliquer des filtres sur des flux
    
    // stream_filter_register() => Enregistre un filtre personnalisé
    