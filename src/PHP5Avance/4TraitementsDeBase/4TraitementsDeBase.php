<?php
    echo "Chapitre 4 : Traitements de base<br />";
    echo "------------------------------------------------------<br />";
    
    // Affeectation par copie et rérérence
    $origine = 1;
    $copie   = &$origine;
    $copie   = $copie + 1;
    echo "<br />copie : $copie"; // Affiche 2
    echo "<br />origine : $origine"; // Affiche 2
    
    $origine   = 1;
    $reference = &$origine;
    $origine   = 2;
    echo "<br />reference : $reference"; // Affiche 2
    echo "<br />origine : $origine"; // Affiche 2
    unset($origine);
    echo "<br />reference apres unset(origine) : $reference"; // Affiche 2
    echo "<br />origine apres unset(origine) : $origine"; // Affiche une notice undefined variable
    
    // Retourner plusieurs variables
    function retournerVariables() 
    {
        $tab[] = "Meek";
        $tab[] = "Mill";
        $tab[] = 06522;
        return $tab;
    }
    $tab = retournerVariables();
    echo "<br /> nom : $atb[0]<br />prenom : $tab[1]<br />tel:$tab[2]<br />";
    list($nom,$prenom,$tel)= $tab;
    echo "<br /> nom : $nom<br />prenom : $prenom<br />tel:$tel<br />";
    
    function retournerVariables1()
    {
        return array('nom' => 'Meek', 'prenom' => 'Mill', 'tel' => 11111);
    }
    $ext = retournerVariables1();
    $retour = extract($ext);
    echo "<br />extraction : ";
    echo "<br /> nom : $nom<br />prenom : $prenom<br />tel:$tel<br />";
    
    // Nombre de paramètres indéfini
    function indefinie()
    {
        echo "<br />Il y a eu ", func_num_args(), ' arguments : ';
        $tab = func_get_args();
        echo "<br />",implode(',', $tab);
    }
    
    indefinie();
    indefinie('111', '222', '333');
     
    // Fonctions anonymes
    $fonction = function () 
    {
        echo "<br /><br />Ceci est une fonction anonyme";
    };
    
    $fonction();
    
    // Fermeture lexicale
    $fermeture = "Ceci est une ";
    $lexicale  = "fermeture lexicale";
    
    $fonction  = function () use ($fermeture, $lexicale) {
        echo "<br /><br />", $fermeture . ' ' . $lexicale;
    };
    
    $fonction();
