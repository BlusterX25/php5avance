<?php
    echo "Chapitre 5 : Traitements de chaines";
    echo "<br />--------------------------------------------------------<br /><br />";
    
    // Affichage avec masque
    $version = 5;
    $miroir  = "fr";
    $masque  = "PHP %d est disponible sur http://%s.php.net";
    printf($masque, $version, $miroir);
    echo "<br />";
    printf("Un entier a trois chiffres : %03d",1);
    echo "<br />";
    printf("%-4sB",'A');
    $texte = sprintf("%-4sB",'B');
    echo "<br />$texte";
    $data = array("PHP",5);
    vprintf('<br />%s %d', $data);
    $data = vsprintf('<br />%s %d', $data);
    echo "$data";
    $texte = '3 arrondi de 3.14';
    list($entier, $chaine, $decimal) = sscanf($texte, '%d %s de %f');
    echo "<br />$entier";
    echo "<br />$chaine";
    echo "<br />$decimal";
    $texte = 'PHP 5 avanc&eacute;';
    echo "<br />" . $texte{1};
    echo "<br />" .  ord('a'); // Valeur ASCII de a (ordinal value)
    echo "<br />" . chr(97); // renvoie un caractère à partir de son code ASCII
    $texte = 'Ceci est du PHP';
    echo "<br />Longueur de la chaine \"Ceci est du PHP \" : " . strlen($texte);
    // En UTF 8 certains caractères accentués prennent deux octet au lieu de 1
    echo "<br />Nombre de mots de la chaine \"Ceci est du PHP\" : " . str_word_count($texte);
    $tab = str_word_count($texte, 1); // Avec le second paramètre à 1 on retourne tous les mots
    echo "<br />Tous les mots de la chaine \"Ceci est du PHP\" : ";
    print_r($tab);
    $tab = str_word_count($texte, 2); // Avec le second paramètre à 2 on retourne tous les mots et l'index est la postion 
    // du premier caractère du mot dans la chaine
    echo "<br />Tous les mots de la chaine \"Ceci est du PHP\" : ";
    print_r($tab);
    $pos = strpos('eric@daspet.php', '@'); // Un 3ieme parametre pr le numero à partir duquel il faut commencer la recherche
    if ($pos === FALSE ) {
        echo "<br />la sous chaine n'apparait pas dans la chaine";
    } else {
        echo "<br />le caractere @ est present a la position $pos";
    }

    /*
     * stripos : fait la même chose que strpos mais permet de ne pas tenir compte de la casse
     * strrpos : permet de faire la recherche de droite à gauche au lieu de gauche à droite
     * strripos : recherche de droite à gauche et ne tient pas compte de la casse
     */
   
   $chaine = 'Chaine à vérifier';
   $masque = '@';
   if (strcspn($chaine, $masque) == strlen($chaine)) {
       echo "<br />La sous-chaine n'est pas presente";
   } else {
       echo "<br />Il y a des @";
   } 
   $nom = "Pierre de Geyer d'Orth";
   echo "<br /> nom : " . $nom;
   $nom = addslashes($nom);
   echo "<br /> nom apres addslashes() : " . $nom;
   // addcslashes() est la même fonction plus étendue, elle convertie aussi les fins de ligne et les retours chariots
   $nom = stripslashes($nom);
   echo "<br />nom apres stripslashes() : " . $nom;
   
   $texte = "valeur avec & <br /> et avec \" et '";
   echo "<br />Ne converti rien, tout est interprete : ";
   echo $texte;
   echo "<br />Convertit les caracteres &, >, < et \" : ";
   echo htmlspecialchars($texte);
   echo "<br />Convertit les caracteres &, >, < et \" : ";
   echo htmlspecialchars($texte, ENT_COMPAT);
   echo "<br />Convertit les caracteres &, >, <, \" et ' : ";
   echo htmlspecialchars($texte, ENT_QUOTES);
   $string = "ligne 1 \n ligne 2";
   $texte = '<h1><a href="page.html">titre</a></h1>';
   echo "<br />Texte brute : $texte ";
   echo "<br />Texte protege : ";
   echo strip_tags($texte, '<h1><em><strong>');
   echo "<br />" . nl2br($string);
   // Bibliothèque intl qui permet d'utiliser la collation
   /*$chaine1 = 'Bonjour';
   $chaine2 = 'bonjour';
   $collator = new Collator('fr_FR');
   $comparaison = $collator->compare($chaine1, $chaine2);
   if ($comparaison === false) {
       echo "<br />" . $collator->getErrorMessage();
   } elseif ($comparaison > 0) {
       echo "<br />" . $chaine1 . " est plus grande que " . $chaine2;
   } elseif ($comparaison < 0) {
       echo "<br />" . $chaine1 . " est plus petite que " . $chaine2;
   } else {
       echo "<br />" . $chaine1 . " est egale a " . $chaine2;
   }*/
   $chaine = 'eric@daspet.php';
   echo "<br />strstr  (eric@daspet.php, @) : " . strstr($chaine, '@');
   echo "<br />substr('eric@daspet.php',2,3) : " . substr($chaine, 2, 3);
   $texte    = 'PHP 4 avance';
   $cherche  = '4';
   $remplace = '5';
   echo "<br />str_replace() : " . str_replace($cherche, $remplace, $texte);
   $texte    = 'PHP 4 debutant';
   $cherche  = array('4', 'debutant');
   $remplace = array('5', 'avance');
   echo "<br />str_replace() avec tableau : " . str_replace($cherche, $remplace, $texte);
   // Si on rajoute un 4ieme argument, il sera pris par référence et contiendra le nb de remplacement fait
   
   // Fonction d'élagage
   $texte = 'PHP 5 avance';
   echo "<br />strlen(chaine) : " . strlen($texte);
   $texte = trim($texte);
   echo "<br />strlen(chaine) apres elagage trim : " . strlen($texte);
   // ON peut spécifier en deuxième paramètre la liste des caractères à supprimer
   // rtrim() ne retire que les espaces à droite, ltrim() ceux de gauche
   
   // Fonction de remplissage str_pad('PHP', 10, ' ', 'STR_PAD_BOTH')
   
   // strtoupper() renvoie des majuscules
   
   // strtolower() renvoie des minuscules
   
   // ucfirst() met la première lettre en majuscule
   
   // ucwords() met les premières lettres de chaque mot en majuscule
   
   // Coupure de paragraphe
   $texte = 'Avec des majuscules et des minuscules';
   echo "<br />" . wordwrap($texte, 5, '<br />', FALSE);
