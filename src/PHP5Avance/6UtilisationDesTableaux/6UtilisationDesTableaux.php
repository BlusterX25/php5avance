<?php
    echo "Chapitre 6 : Utilisation des tableaux";
    echo "<br />-------------------------------------------------------------<br /><br />";
    
    // Taille d'un tableau
    $tab = array(1,3,5);
    echo "<br />Taille du tableau : " . count($tab);
    // count() renvoie 1 qd on y fait appel en lui passant une chaine de caractères non vide à la place d'un tableau
    // sizeof() est un alias de count()
    
    // Test d'existence et type d'un tableau
    $colors = array('rouge', 'vert', 'bleu');
    if (isset($colors) && is_array($colors)) {
        echo "<br />C'est un tableau et il existe";
    }
    
    /*
     * Recherche d'un élément 
     */
     
     // Présence dans le tableau
     $caract = array('jeune', 'beau', 'geek', 'drole');
     echo "<br />";
     print_r($caract);
     if (in_array('jeune', $caract)) { // Le dernier paramètre optionnel permet de vérifier si le type est le même
         echo "<br />Trouv&eacute; jeune";
     }
     if(in_array('geek', $caract)) {
         echo "<br /> Trouv&eacute; geek";
     }
     
     // Recherche de la clé correspondante
     $cle = array_search('jeune', $caract);
     echo "<br />Le mot jeune est &agrave; l'indice " . $cle;
     if ( array_search('jeune', $caract) !== FALSE) {
         echo "<br />Le caract&egrave;re jeune est toujours pr&eacute;sent";
     }
     
     $couleur = array('ff0000' => 'rouge', '00ff00' => 'vert', '0000ff' => 'bleu');
     if (array_key_exists('00ff00', $couleur)) {
         echo "<br />La cl&eacute; '00ff00' existe";
     }
     
     // Nombre d'occurences d'un élément
     $tab = array('cyril', 'christophe', 'cyril', 'thomas', 'eric');
     echo "<br />Tableau de pr&eacute;noms : ";
     print_r($tab);
     $count = array_count_values($tab);
     echo "<br />array_count_values() : ";
     print_r($count);
     
     //Récupération aléatoire d'un élément
     $tab  = array(1,2,3,4,5,6);
     $rand = array_rand($tab, 2); // Retourne directement l'indice de la valeur trouvée si pas de 
     // second argument(mais pas dans un tableau)
     echo "<br />Tableau : ";
     print_r($tab);
     echo "<br />Cl&eacute;s des &eacute;l&eacute;ments tir&eacute;s : ";
     print_r($rand); 
     echo "<br />Tirage 1 : " . $tab[$rand[0]];
     echo "<br />Tirage 2 : " . $tab[$rand[1]];
     
     // Trier les tableaux
     echo "<br />tableau caract : ";
     print_r($caract);
     sort($caract); 
     echo "<br />tableau caract apres sort() : ";
     print_r($caract);
     // 2nd paramètre SORT_NUMERIC, SORT_STRING, SORT_REGULAR (par défaut)
     // rsort() => trie inverse
     // asort() et arsort() => conservent les associations clé-valeur
     // ksort() et krsort() => trie se fait sur les clés
     $tab = array('texte1', 'texte3', 'texte2', 'texte12');
     echo "<br />tab : ";
     print_r($tab);
     sort($tab);
     echo "<br />tab apres sort() : ";
     print_r($tab);
     sort($tab);
     natsort($tab);
     echo "<br />tab apres natsort() : ";
     print_r($tab);
     // natcasesort() => comme natsort() mais fait une comparaison insensible à la casse
     // usort($tab,'cmp') => Trier avec fonction utilisateur cmp
     // uksort($tab, 'cmp') => Trier avec fonction utilisateur cmp mais sur les clés
     // array_multisort() => Tri multicritères
     
     // Extraction et remplacement
     $tab = array(1,2,3,4);
     echo "<br />tab : ";
     print_r($tab);
     list($a,$b,$c,$d) = $tab;
     echo "<br />$a-$b-$c-$d";
     $tabAssoc = array('e'=>5, 'f'=>6, 'g'=>7, 'h'=>8);
     extract($tabAssoc);
     echo "<br />$e-$f-$g-$h";
     
     // Sérialisation de tableau
     $chaine = implode('*', $tab);
     echo "<br />Implode : " . $chaine;
     echo "<br />Explode : ";
     print_r(explode("*", $chaine));
     
     // Extraction d'un sous-tableau
     // array_slice() fonctionne comme substr() pour les tableaux
     
     // Remplacement d'un sous-tableau
     // array_splice() fonctionne comme substr_replace() pour les tableaux
     
     // Gestion des clés et des valeurs
     // array_keys($tab) retourne un tableau indéxé numériquement qui contient la liste des clés
     // array_values($tab) fonctionne comme array_keys mais retourne la liste de valeurs
     // array_flip($tab) => Les clés deviennent valeurs et les valeurs deviennent clés
     
     // Fusions et séparations
     $result_2002 = array(12250,12000,21300,25252,20010,8460);
     echo "<br />Result 2002 : ";
     print_r($result_2002);
     $result_2003 = array(1520,25000,13530,1052,5010,3680);
     echo "<br />Result 2003 : ";
     print_r($result_2003);
     $result_2002_2003 = array_merge($result_2002, $result_2003);
     echo "<br />Result 2002-2003 : ";
     print_r($result_2002_2003); 
     // Il est possible de fusionner avec +
     // Si les clés sont associatives, dans le cas on on a le même index lar dernière valeur écrase la précédente
     // array_merge_recursive() => Merge récursif si les tableaux contiennent des tableaux
     
     // Séparation des tableaux en plusieurs
     $tab = array(1,2,3,4,5,6,7);
     echo "<br />tab : ";
     print_r($tab);
     $tabs = array_chunk($tab, 2);
     echo "<br />array_chunk(tab,2) : ";
     print_r("<pre>");
     print_r($tabs);
     print_r("</pre>");
     
     $tabs = array_chunk($tab, 2, TRUE); // Conserve clé-valeur
     echo "<br />array_chunk(tab,2) : ";
     print_r("<pre>");
     print_r($tabs);
     print_r("</pre>");
     
     // Différences et intersections
     // array_diff() retourne la différence entre plusieurs tableaux
     // array_diff_assoc() retourne la différence mais vérifie aussi la correspondance des clés
     // array_intersetc() => retourne la liste des éléments du premier tableau qui sont présents ds tous les autres tableaux
     // array_intersect_assoc => même chose que array_intersect() mais conserve clé-valeur
     // array_unique() => enlève les doublons
     
     // Gestion des piles et des files
     $tab = array();
     echo "<br />tab vide : ";
     print_r($tab);
     array_push($tab, 1,2,3);
     echo "<br />tab apres push : ";
     print_r($tab);
     echo "<br />pop de tab : " . array_pop($tab); // Renvoie la dernière valeur du tableau
     
     // Il est possible de gérer des listes avec array_unshift() et array_shift()
     $tab = array();
     echo "<br />tab vide : ";
     print_r($tab);
     array_unshift($tab, 1,2,3);
     echo "<br />tab apres unshift : ";
     print_r($tab);
     echo "<br />pop de tab : " . array_shift($tab);
     
     // SplStack
     $pile = new SplStack();
     echo "<br />pile vide : ";
     print_r($pile);
     $pile->push(1);
     $pile->push(2);
     $pile->push(3);
     echo "<br />pile apres push : ";
     print_r($pile);
     echo "<br />derniere valeur : " . $pile->pop();
     echo "<br />pile apres pop : ";
     print_r($pile);
     
     // SplQueue avec unshift pour ajouter un élément et shift pour enlever le premier élément
     
     // Navigation dans les tableaux
     // reset(), next(), prev(), end() et current()
     // list($a,$b) = each($tab) retourne clé-valeur courant de tab
     
     
    
    
?>