<?php
    require '../../../../vendor/Transport/Terrestre/Routier/Voiture.php';
    require '../../../../vendor/Transport/Roue.php';
    
    use Transport\Terrestre\Routier\Voiture;
    use Transport\Terrestre\Routier\Vehicule;
    use Transport\Moteur;
    use Transport\Phare;
    use Transport\Roue;
    use Transport\Pneu;
    use Equipement\Mecanique\Jante;
    
    echo "<br />Chapitre 12 : Gestion des objets";
    echo "<br />------------------------------------------<br />";
    
    echo "<br /><br />------------------------------ Ecosysteme automobile --------------------------------------";
    try {
        $annee  = new DateTime('2010-06-20');
        $moteur = new Moteur();
        $phares = array();
        $roues  = array();
        $pharesA = array();
        $rouesA  = array();
        for ($i=0; $i < 4; $i++) {
            $phares[]  = new Phare();
            $pharesA[] = new Phare();
            $pneu      = new Pneu();
            $jante     = new Jante();
            $roues[]   = new Roue($pneu, $jante);
            $rouesA[]  = new Roue(clone $pneu, clone $jante);
        }
        
        //print_r("<pre>");print_r($roues);print_r("</pre>");die;
        $voiture = new Voiture($moteur, $phares, $roues, 'MERCEDES', 'Classe C', $annee);
        $voiture->klaxonner();
        echo "<br />Objet voiture :";
        echo $voiture;
        echo "<br />get_class : " . get_class($voiture);
        echo "<br />get_parent_class : " . get_parent_class($voiture);
        echo "<br />get_class_methods : ";
        print_r('<pre>');print_r(get_class_methods($voiture));print_r('</pre>');
        $anneeA = clone $annee;
        $anneeA->setDate('2011', '11', '26');
        $voitureA = new Voiture($moteur, $pharesA, $rouesA, 'AUDI', 'A4', $anneeA);
        $voiture->faireUnAccident($voitureA);
        echo "<br /> Getter magique : " . $voiture->marque;
        $voiture->marque = 'MERCO';
        echo "<br /> Getter magique apres setter: " . $voiture->marque;
    } catch (\Exception $e) {
        echo "<br /><br />" . $e->getMessage();
        exit(-1);
    }
    
    
