<?php
    require '../../../../vendor/Banque/CompteEnBanque.php';
    
    use Automobile\Voiture;
    use Banque\CompteEnBanque;
    use Jardinage\TondeuseGazon;
    use Automobile\Vehicule;
    
    echo "<br />Chapitre 12 : Gestion des objets";
    echo "<br />------------------------------------------<br />";
    
    echo "<br /><br />------------------------------ Ecosysteme bancaire ------------------------------------------";
    $monCompte = new CompteEnBanque();
    echo "<br />Mon compte " . $monCompte;
    $monCompte->setSolde(3200);
    echo "<br />Mon compte " . $monCompte;
    $compteA = new CompteEnBanque();
    echo "<br />Compte A" . $compteA;
    $monCompte->virer(200, $compteA);
    echo "<br />Mon compte " . $monCompte;
    echo "<br />Compte A " . $compteA;
