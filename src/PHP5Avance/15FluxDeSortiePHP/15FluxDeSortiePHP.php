<?php
    // Demande que tout affichage soit mis en tampon
    
    ob_start();
    echo "<br /> Chapitre 15 : Flux de sortie PHP";
    echo "<br />----------------------------------------------------";
    
    // Le texte est mis en tampon, le visiteur ne reçoit rien
    echo "<br /><br />Demande d'affichage";
    
    // On peut encore envoyer un cookie sans erreur car le serveur web n'a encore 
    // envoyé aucun contenu. Le bloc d'entête n'est toujours pas finalisé
    setcookie('nom','valeur');
    
    // PHP envoie le contenu du tampon au serveur web. La mise en tampon est arrêtée.
    // Le client reçoit : essai d'affichage. Le cookoe est bien reçu aussi
    ob_end_flush();
    
    // ob_end_clean() => vide le tampon sans rien envoyé au navigateur
    // ob_get_clean() => vide le tampon et retourne son contenu
    
    // ob_start('ob_gzhanler') => Envoie de page compressé
    // ob_gzhanler() => Interne à zlib et permet de compresser une nouvelle chaine de caractère
    
    // ob_start('mb_output_handler') => Pour utiliser une conversion de codage mb_string
    
    // iconv_set_encoding('internal_encoding', 'ISO-8859-1') => encodage en interne
    // iconv_set_encoding('internal_encoding', 'UTF-8') => jeu d'encodage de destination
    // ob_start('ob_iconv_handler') ou ob_iconv_handler() => Pour la conversion vers un jeu de caractère
    
    
    
    