<?php
    echo "Chapitre 8 : Formulaires et superglobales";
    echo "<br />------------------------------------------<br />";
    
    // Caractères spéciaux HTML
    echo "<br />htmlentities : " . htmlentities("L'affirmation 1<2 est vraie, pas 1>2");
    $defaut = "c'est moi";
    $defaut = htmlentities($defaut, ENT_QUOTES);
    echo "<br /><input name='defaut' type='text' value='$defaut'>";
    
    // Méthode d'envoi du formulaire
    // Methode GET, transmission par url
    $nom    = urlencode("daspet");
    $prenom = urlencode("éric");
    $lien   = "8FormulairesEtSuperglobales.php?nom=$nom&prenom=$prenom";
    echo "<br /><a href='$lien'>Lien</a>";

    if (isset($_GET['nom'])) {
        echo "<br />Nom : " . $_GET['nom'];
    }
    if (isset($_GET['prenom'])) {
        echo "<br />prenom : " . $_GET['prenom'];
    }
    
    // Méthode POST
    if (isset($_POST['variable'])) {
        echo "<br />variable post : " . $_POST['variable'];
    }
    
    // Validation de données avec l'extension filter
    $nom_f = filter_input(INPUT_GET, 'nom');
    if ($nom_f === FALSE) {
        echo "<br />filter_input : Le nom n'est pas valide";
    } elseif ($nom_f === NULL) {
        echo "<br />filter_input : Le nom n'a pas ete fourni";
    } else {
        echo "<br />filter_input : le nom est => " . $nom_f;
    }
    
    $prenom_f = filter_input(INPUT_GET, 'prenom');
    if ($prenom_f===FALSE) {
        echo "<br />filter_input : Le prenom n'est pas valide";
    } elseif ($prenom_f===NULL) {
        echo "<br />filter_input : Le prenom n'est pasa fourni";
    } else {
        echo "<br />filter_input : Le prenom est => " . $prenom_f;
    }
    
    $variable = filter_input(INPUT_POST, 'variable');
    if ($variable===FALSE) {
        echo "<br />filter_input : La variable n'a pas valide";
    } elseif ($variable===NULL) {
        echo "<br />filter_input : La variable n'a pas ete fournie";
    } else {
        echo "<br />filter_input : La variable poste est => " . $variable;
    }
    
    $adresse ="Bonjour <b>Eric</b>";
    echo "<br />adresse avant filter_var : " . $adresse;
    $message = filter_var($adresse, FILTER_SANITIZE_SPECIAL_CHARS); // Filtre les caractères spéciaux
    echo "<br />message apres filter_var : " . $message;
    
    // filter_var_array() se comporte comme filter_var() mais il retourne un tableau de valeurs filtrés
    
    // Voir les filtres les plus courants
    if (filter_has_var(INPUT_POST, 'variable')) {
        $variable = filter_input(INPUT_POST, 'variable');
        echo "<br />filter_has_var puis filter_input : variable => " . $variable;
    }
?>
<br />
<form action="8FormulairesEtSUperglobales.php" method="POST">
    <p>
        Entrez du texte : <br />
        <input name="variable" type="text"><br />
        <input name="envoyer" type="submit">
    </p>
</form>
