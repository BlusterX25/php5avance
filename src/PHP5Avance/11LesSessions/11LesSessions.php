<?php
    session_start();
    // Si on met session_readonly()  à la place de session_start() => impossible d'écrire en session
    // Definition de 'cleint' comme nom de session (par défaut le nom est PHPSESSID)
    session_name('client');
    // et de '/tmp' comme répertoire de stockage des sessions
    session_save_path('/tmp');
    //phpinfo();die;
    echo "<br />Chapitre 11 : Les sessions";
    echo "<br />-------------------------------<br />";
    
    $_SESSION['langage'] = "PHP 5";
    
    if (isset($_SESSION['langage'])) {
        echo "<br />Avec SESSION : La session langage existe : " . $_SESSION['langage'];
    } else {
        echo "<br />Avec SESSION : La session langage n'existe pas ";
    }
    if (filter_has_var(INPUT_SESSION, 'langage')) {
        $langage = filter_input(INPUT_SESSION, 'langage');
        echo "<br />Avec filter : La session langage existe : " . $langage;
    } else {
        echo "<br />Avece filter : La session langage n'existe pas";
    }
    
    $tab = array('un', 'deux', 'trois', 'quatre');
    $_SESSION['tab'] = $tab;
    print_r('<pre>');
    print_r($_SESSION);
    print_r('</pre>');
     
    echo "<br />Le nom de la session : " . session_name();
    
    echo "<br />Identifiant utilise : " . session_id();
    echo "<br />Session save path : " . session_save_path();
    
    // On détruit la session
    session_destroy();
    unset($_SESSION);
    echo "<br />Avec SESSION apres destroy et unset : " . $_SESSION['langage'];
    
    // On peut définir un gestionnaire de session personnalisé
    // session_set_save_handler(
    //      'init', 'ferme', 'lit', 'ecrit', 'efface', 'nettoie'
    // );
    // init, ferme, lit, ecrit, efface, nettoie sont des méthodes que l'utilisateur doit implémenter
    
    // SECURITE : Attaque par fixation de session
    // Quand on utilise de l'authentification il faut faire un session_regenerate_id() pour ne pas que l'attaquant fournisse un numéro de session
    // connu avant que la victime s'authentifie
    