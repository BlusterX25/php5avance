<?php
    namespace Transport;
    
    require 'Eclairage.php';
    
    use Transport;
    
    class Phare extends Eclairage
    {
        public function allumer()
        {
            echo "<br />Allumer Phare";
        }
        
        public function eteindre()
        {
            echo "<br />Eteindre Phare";
        }
    }