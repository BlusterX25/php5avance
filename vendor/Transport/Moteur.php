<?php
    namespace Transport;
    
    //require 'AllumableInterface.php';
    
    use Equipement\Electrique\AllumableInterface;
    
    class Moteur implements AllumableInterface
    {
        public function allumer()
        {
            echo "<br />Allumer Moteur";
        }
        
        public function eteindre()
        {
            echo "<br />Eteindre Moteur";
        }
    }
    