<?php
    namespace Transport;
    
    require '../../../../vendor/Transport/Pneu.php';
    require '../../../../vendor/Equipement/Mecanique/Jante.php';
    
    use Transport\Pneu;
    use Equipement\Mecanique\Jante;
    
    class Roue
    {
        private $jante;
        private $pneu;
        
        public function __construct(Pneu $pneu, Jante $jante)
        {
            $this->pneu  = $pneu;
            $this->jante = $jante;
        }
        
        /**
         * 
         * @param string $nom
         * @return unknwon
         * @throws \Exception
         */
        public function __get($nom)
        {
            $attributs = get_class_vars(get_class($this));
            if (array_key_exists($nom, $attributs)) {
                return $this->{$nom};
            } else {
                throw new \Exception("<br />ACCESSEUR ROUE : L'attribut $nom n'est pas d&eacute;fini dans la classe " . __CLASS__);
            }
        }
        
        /**
         * 
         * @param unknown $nom
         * @param unknown $valeur
         * @throws \Exception
         * @return \Transport\Roue
         */
        public function __set($nom, $valeur)
        {
            $attributs = get_class_vars(get_class($this));
            if (array_key_exists($nom, $attributs)) {
                $this->{$nom} = $valeur;
                return $this;
            } else {
                throw new \Exception("<br />MUTATEUR ROUE : L'attribut $nom n'est pas d&eacute;fini dans la classe " . __CLASS__);
            }
        }
    }