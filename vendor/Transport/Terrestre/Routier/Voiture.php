<?php
    /**
     * 
     * @author chronic
     *
     */
    namespace Transport\Terrestre\Routier;
    
    /**
     * 
     */
    require 'Vehicule.php';
    //require '../../AllumableInterface.php';
    require '../../../../vendor/Equipement/Electrique/AllumableInterface.php';
    require '../../../../vendor/Transport/Moteur.php';
    require '../../../../vendor/Transport/Phare.php';
    
    
    
    use Transport\Terrestre\Routier\Vehicule;
    use Equipement\Electrique\AllumableInterface;
    use Transport\Moteur;
    //use Transport\Eclairage;
    use Transport\Phare;
    use Transport\Roue;
    
    class Voiture extends Vehicule implements AllumableInterface
    {
        /**
         * 
         * @var Moteur
         */
        private $moteur;
        
        /**
         * 
         * @var array<Phare>
         */
        private $phares;
        
        /**
         * 
         * @var string
         */
        private $marque;
        /**
         * 
         * @var string
         */
        private $modele;
        /**
         * 
         * @var \DateTime
         */
        private $annee;
        
        /**
         * 
         * @var Array<Roue>
         */
        private $roues;
        
        
        public function __construct(Moteur $moteur, $phares, $roues, $marque = NULL, $modele = NULL, \DateTime $annee = NULL)
        {
            parent::__construct();
            $this->moteur = $moteur;
            if (is_array($phares) && count($phares) == 4) {
                foreach ($phares as $phare) {
                    if (!($phare instanceof Phare)) {
                        throw new \Exception("CONSTRUCTION VOITURE : Ce n'est pas un phare");
                    }
                }
                $this->phares = $phares;
            } else {
                throw new \Exception("CONSTRUCTION VOITURE : La voiture doit avoir quatres phares");
            }
            
            //print_r("<pre>");print_r($roues);print_r("</pre>");die;
            
            if (is_array($roues) && count($roues) == 4) {
                foreach ($roues as $roue) {
                    if (!($roue instanceof Roue)) {
                        throw new \Exception("CONSTRUCTION VOITURE : Ce n'est pas une roue");
                    }
                }
                $this->roues = $roues;
            } else {
                throw new \Exception("CONSTRUCTION VOITURE : La voiture doit avoir quatres roues");
            }
            
            $this->marque = $marque;
            $this->modele = $modele;
            $this->annee  = $annee;
        }
        
        /**
         * 
         * @param string $nom
         * @return string
         * @throws \Exception
         */
        public function __get($nom)
        {
            $attributs = get_class_vars(get_class($this));
            if (array_key_exists($nom, $attributs)) {
                return $this->{$nom};
            } else {
                throw new \Exception("<br />ACCESSEUR VOITURE : L'attribut $nom n'est pas d&eacute;fini dans la classe " . __CLASS__);
            }
        }
        
        /**
         * 
         * @param string $nom
         * @param unknown $valeur
         * @throws \Exception
         * @return \Transport\Terrestre\Routier\Voiture
         */
        public function __set($nom, $valeur)
        {
            $attributs = get_class_vars(get_class($this));
            $except    = array('marque', 'modele', 'annee');
            if (array_key_exists($nom, $attributs)) {
                if (in_array($nom, $except)) {
                    throw new \Exception("<br />MUTATEUR VOITURE : L'attribut $nom ne peut pas &ecirc;tre modifi&eacute; dans la classe " . __CLASS__);
                }
                $this->{$nom} = $valeur;
                return $this;
            } else {
                throw new \Exception("<br />MUTATEUR VOITURE : L'attribut $nom n'est pas d&eacute;fini dans la classe " . __CLASS__);
            }
        }
        
        /**
         *
         * @param int $temps
         */
        public function avancer($temps)
        {
            $distance = $temps* $this->vitesse;
            echo "<br />Pendant ces $temps heures on a avanc&eacute; de $distance km";
        }
        
        public function arreter()
        {
            
        }
        
        /**
         * 
         * @param int $foreDeFreinage
         */
        public function freiner($foreDeFreinage)
        {
        }
        
        /**
         * 
         */
        public function klaxonner()
        {
            echo "<br />Vous klaxonnez fort!";
        }
        
        public function faireUnAccident(Voiture $voiture)
        {
            echo "<br />IL Y A EU ACCIDENT ENTRE LA VOITURE ";
            echo $this;
            echo "<br />ET LA VOITURE ";
            echo $voiture;
        }
        
        public function allumer()
        {
            
        }
        
        public function eteindre()
        {
            
        }
        
        /**
         * 
         * @return string
         */
        public function __toString()
        {
            $chaine  = "<br />&nbsp;&nbsp;&nbsp;&nbsp;(Namespace, Classe, Methode) = (" . __NAMESPACE__ .", " .  __CLASS__ . ", " . __METHOD__ .")"; 
            $chaine .= "<br />&nbsp;&nbsp;&nbsp;&nbsp;Marque : " .$this->marque;
            $chaine .= "<br />&nbsp;&nbsp;&nbsp;&nbsp;Model : " . $this->modele;
            $chaine .= "<br />&nbsp;&nbsp;&nbsp;&nbsp;Annee : " . $this->annee->format('d/m/Y');
            return $chaine;
        }
        
        public function __destruct()
        {
            echo "<br />Appel au destructeur de la classe " . __CLASS__;
            //parent::__destruct(); //Si on le met il il cherche à détruire une fois de plus la classe Vehicule
        }
        
    }
