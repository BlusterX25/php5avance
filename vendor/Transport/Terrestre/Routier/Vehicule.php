<?php
    
    /**
     * 
     * @author chronic
     *
     */

    namespace Transport\Terrestre\Routier;
    
    abstract class Vehicule
    {
        /**
         * 
         * @var int
         */
        private $vitesse;
        
        protected function __construct($vitesse = 0)
        {
            $this->vitesse = $vitesse;
        }
        
        /**
         * 
         * @param int $temps
         */
        abstract public function avancer($temps);
        
        abstract public function arreter();
        
        public function __destruct()
        {
            echo "<br />Appel au destructeur de la classe " . __CLASS__;
        }
    }