<?php
    namespace Transport;
    
    require '../../../../vendor/Equipement/Electrique/Ampoule.php';    
    
    use Equipement\Electrique\Ampoule;
    use Equipement\Electrique\AllumableInterface;
    
    
    abstract class Eclairage extends Ampoule implements AllumableInterface
    {
        private $type;
        
        /**
         * 
         * @param string $nom
         * @throws \Exception
         */
        public function __get($nom)
        {
            $attributs = get_class_vars(get_class($this));
            if (array_key_exists($nom, $attributs)) {
                return $this->{$nom};
            } else {
                throw new \Exception("<br />ACCESSEUR ECLAIRAGE : L'attribut $nom n'est pas d&eacute;fini dans la classe " . __CLASS__);
            }
        }
        
        /**
         * 
         * @param string $nom
         * @param unknown $valeur
         * @throws \Exception
         * @return \Transport\Eclairage
         */
        public function __set($nom, $valeur)
        {
            $attributs = get_class_vars(get_class($this));
            if (array_key_exists($nom, $attributs)) {
                $this->{$nom} = $valeur;
                return $this;
            } else {
                throw new \Exception("<br />MUTATEUR ECLAIRAGE : L'attribut $nom n'est pas d&eacute;fini dans la classe " . __CLASS__);
            }
        }
        
        abstract public function allumer();
        
        abstract public function eteindre();
    }