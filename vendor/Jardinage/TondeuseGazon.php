<?php
    /**
     * 
     * @author chronic
     *
     */
    namespace Jardinage;
    
    class TondeuseGazon
    {
        const TRACTEE    = 1;
        const AUTOPORTEE = 2;
        const POUSSEE    = 4;
        
        /**
         * 
         * @var int;
         */
        public $type;
        
        /**
         * 
         */
        public function setTondeusePoussee()
        {
            $this->type = self::POUSSEE;
        }
    }

