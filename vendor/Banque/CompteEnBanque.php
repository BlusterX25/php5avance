<?php
    /**
     * 
     * @author chronic
     *
     */
    namespace Banque;
    
    class CompteEnBanque
    {
        /**
         * 
         * @var float
         */
        private $solde = 0;
        
        /**
         * 
         * @param float $valeur
         * @param CompteEnBanque $destination
         */
        public function virer($valeur, CompteEnBanque $destination)
        {
            $this->solde        -= $valeur;
            $destination->solde += $valeur;
        }
        
        /**
         * 
         * @return number
         */
        public function getSolde()
        {
            return $this->solde;
        }
        
        /**
         * 
         * @param float $solde
         * @return \Classes\CompteEnBanque
         */
        public function setSolde($solde)
        {
            $this->solde = $solde;
            return $this;
        }
        
        /**
         * 
         * @return string
         */
        public function __toString()
        {
            return "<br />&nbsp;&nbsp;&nbsp;&nbsp;Solde : " . $this->solde;
        }
    }

