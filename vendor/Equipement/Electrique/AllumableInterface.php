<?php
    namespace Equipement\Electrique;
    
    interface AllumableInterface
    {
        public function allumer();
        public function eteindre();
    }