<?php 
?>
<html>
    <head>
    </head>
    <body>
        <h1>PHP5 avanc&eacute; 6&egrave;me &eacute;dition</h1>
        <div class="sidebar-nav">
            <div class="well" style="width:300px; padding: 8px 0;">
                <ul class="nav nav-list"> 
                    <li><a href="/src/PHP5Avance/2InstallerEtConfigurer/2InstallerEtConfigurer.php">Chapitre 2 : Installer et Configurer</a></li>
                    <li><a href="/src/PHP5Avance/3StructuresDeBase/3StructuresDeBase.php">Chapitre 3 : Structures de base</a></li>
                    <li><a href="/src/PHP5Avance/4TraitementsDeBase/4TraitementsDeBase.php">Chapitre 4 : Traitements de base</a></li>
                    <li><a href="/src/PHP5Avance/5TraitementsDeChaines/5TraitementsDeChaines.php">Chapitre 5 : Traitements de chaines</a></li>
                    <li><a href="/src/PHP5Avance/6UtilisationDesTableaux/6UtilisationDesTableaux.php">Chapitre 6 : Utilisation des tableaux</a></li>
                    <li><a href="/src/PHP5Avance/7FonctionsUsuelles/7FonctionsUsuelles.php">Chapitre 7 : Fonctions usuelles</a></li>
                    <li><a href="/src/PHP5Avance/8FormulairesEtSuperglobales/8FormulairesEtSuperglobales.php">Chapitre 8 : Formulaires et superglobales</a></li>
                    <li><a href="/src/PHP5Avance/9EnvironnementWebEtSuperglobales/9EnvironnementWebEtSuperglobales.php">Chapitre 9 : Environnement web et superglobales</a></li>
                    <li><a href="/src/PHP5Avance/10LesCookies/10LesCookies.php">Chapitre 10 : Les cookies</a></li>
                    <li><a href="/src/PHP5Avance/11LesSessions/11LesSessions.php">Chapitre 11 : Les sessions</a></li>
                    <li><a href="/src/PHP5Avance/12GestionDesObjets/12GestionDesObjets.php">Chapitre 12 : Gestion des objets</a></li>
                    <li><a href="/src/PHP5Avance/13GestionDeFichiers/13GestionDeFichiers.php">Chapitre 13 : Gestion de fichiers</a></li>
                    <li><a href="/src/PHP5Avance/14GestionDesFlux/14GestionDesFlux.php">Chapitre 14 : Gestion des flux</a></li>
                    <li><a href="/src/PHP5Avance/15FluxDeSortiePHP/15FluxDeSortiePHP.php">Chapitre 15 : Flux de sortie PHP</a></li>
                    <li><a href="/src/PHP5Avance/16EnvoyerEtRecevoirDesCourriels/16EnvoyerEtRecevoirDesCourriels.php">Chapitre 16 : Envoyer et recevoir des emails</a></li>
                    <!-- <li><a href="/src/PHP5Avance/12GestionDesObjets/12GestionDesObjets.php">Chapitre 12 : Gestion des objets</a></li> -->
                </ul>
            </div>
        </div>
    </body>
</html>